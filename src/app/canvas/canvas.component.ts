import { Component, OnInit, ElementRef } from '@angular/core';
// import * as PIXI from 'pixi.js';

declare var PIXI:any;

interface Rect {
  name: string
  top: number
  right: number
  bottom: number
  left: number
}

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements OnInit {
  viewport = {
    width: 1040,
    height: 500
  };
  aspectRatio: number;
  app: any;
  rects: Rect[] = [{
    name: 'Theresa Roberts',
    top: 100,
    right: 250,
    bottom: 168,
    left: 200
  }, {
    name: 'Ron Lee',
    top: 110,
    right: 460,
    bottom: 158,
    left: 410
  }];

  // Calculation
  scalar: number = 1;

  constructor(
    private hostElement: ElementRef
  ) { }

  ngOnInit() {
    this.aspectRatio = this.viewport.width / this.viewport.height;

    this.setupApp(this.viewport.width, this.viewport.height);

    const img = new Image();
    img.onload = function(e) {
      this.drawUI(this.rects, e.currentTarget);
    }.bind(this)
    img.src = 'assets/images/people-walking.png';
    this.drawUI(this.rects);
  }

  setupApp(width: number, height: number, options?: object) {
    // The application will create a renderer using WebGL, if possible,
    // with a fallback to a canvas render. It will also setup the ticker
    // and the root stage PIXI.Container
    this.app = new PIXI.Application(
      width,
      height,
      {
        antialias: true,
        ...options
      }
    );
  }

  drawUI(rects: Rect[], backgroundImage?: HTMLImageElement) {
    if (backgroundImage) this.setBackground(backgroundImage);

    for (let i = 0, len = rects.length; i < len; i++) {
      const rect = rects[i];
      this.drawRect(rect);
      this.drawText(rect, rect.name);
    }
  }

  setBackground(image: HTMLImageElement) {
    // The application will create a canvas element for you that you
    // can then insert into the DOM
    this.hostElement.nativeElement.appendChild(this.app.view);
    let imageWidth = image.width;
    let imageHeight = image.height;
    const viewportAspectRatio = this.viewport.width / this.viewport.height;
    const imageAspectRatio = imageWidth / imageHeight;

    // if (viewportAspectRatio > 1) {
    //   this.scalar = this.viewport.width / imageWidth;
    //   imageWidth = this.viewport.width;
    //   imageHeight = imageHeight * this.scalar;
    //
    //   if (imageHeight > this.viewport.height) {
    //     this.scalar = this.viewport.height / imageHeight;
    //     imageHeight = this.viewport.height;
    //     imageWidth = imageWidth * this.scalar;
    //   }
    // } else if (viewportAspectRatio < 1) {
    //   this.scalar = this.viewport.height / imageHeight;
    //   imageHeight = this.viewport.height;
    //   imageWidth = imageWidth * this.scalar;
    //
    //   if (imageWidth > this.viewport.width) {
    //     this.scalar = this.viewport.width / imageWidth;
    //     imageWidth = this.viewport.width;
    //     imageHeight = imageHeight * this.scalar;
    //   }
    // }

    // this.scalar = this.viewport.width / imageWidth;
    // imageWidth = this.viewport.width;
    // imageHeight = imageHeight * this.scalar;
    //
    // if (imageHeight > this.viewport.height) {
    //   this.scalar = this.viewport.height / imageHeight;
    //   imageHeight = this.viewport.height;
    //   imageWidth = imageWidth * this.scalar;
    // }

    console.log(`
      VP -> width: ${this.viewport.width} height: ${this.viewport.height}
      VP -> aspect ratio: ${viewportAspectRatio}
      Image -> width: ${imageWidth} height: ${imageHeight}
      Image -> aspect ratio: ${imageWidth / imageHeight}
      Scalar: ${this.scalar}
      `);
    // const imageAspectRatio = imageWidth / imageHeight;
    // if (imageAspectRatio < 1) {
    //   // Native Portrait Image
    //   if () {
    //
    //   }
    // } else {
    //   // Native Landscape Image
    // }

    // create a new background sprite
    const background = new PIXI.Sprite.fromImage(image.src);
    background.width = imageWidth;
    background.height = imageHeight;
    this.app.stage.addChild(background);
  }

  drawRect(rect: Rect) {
    var graphics = new PIXI.Graphics();

    // set a fill and a line style again and draw a rectangle
    graphics.lineStyle(1, 0x00FF00, 1);
    // graphics.beginFill(0xFF700B, 1);
    graphics.drawRect(
      rect.left * this.scalar,
      rect.top * this.scalar,
      (rect.right - rect.left) * this.scalar,
      (rect.bottom - rect.top) * this.scalar
    );

    this.app.stage.addChild(graphics);
  }

  drawText(rect: Rect, text: string) {
    const fontSize = 14 * this.scalar;
    const topOffsetText = fontSize + 5;

    const style = new PIXI.TextStyle({
      fontFamily: 'Arial',
      fontSize: fontSize,
      fill: ['#ffffff'], // gradient
      stroke: 'rgba(0, 0, 0, 1)',
      strokeThickness: 6
    });

    const richText = new PIXI.Text(text, style);
    richText.x = (rect.left + (rect.right - rect.left) / 2) * this.scalar;
    richText.y = (rect.top - topOffsetText) * this.scalar;
    richText.anchor.set(0.5);

    this.app.stage.addChild(richText);
  }

}
